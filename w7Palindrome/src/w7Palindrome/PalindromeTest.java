package w7Palindrome;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;

import org.junit.jupiter.api.Test;

class PalindromeTest {
	
	@Test
	public void testPalindromeCheck() {
		boolean result = Palindrome.isPalindrome("anna");
		Assert.assertTrue(result);
		
	}
	
	@Test
	public void testPalindromeCheckNegative() {
		boolean result = Palindrome.isPalindrome("andre"); 
		Assert.assertTrue(result);
		
	}
	
	@Test
	public void testPalindromeBoundaryOut() {
		boolean result = Palindrome.isPalindrome("ansaasna"); 
		Assert.assertTrue(result);
		
	}

	@Test
	public void testPalindromeBoundaryIn() {
		boolean result = Palindrome.isPalindrome("aa"); 
		Assert.assertTrue(result);
		
	}

	

}
